<?php

/**
 * @file
 * setefi menu items.
 *
 */


function uc_setefi2_notify($cart_id = 0) {
	$paymentID = $_REQUEST['paymentid'];
	$result = $_REQUEST['result'];
	$auth = $_REQUEST['auth'];
	$ref = $_REQUEST['ref'];
	$tranid = $_REQUEST['tranid'];
	$trackid = $_REQUEST['trackid'];
	$details = $_REQUEST['udf1'];
	$responsecode = $_REQUEST['responsecode'];
	
	
	$payed=FALSE;
	// loading order from ubercart
	$order = uc_order_load($results['trackid']);
	// ERROR - ORDER NOT IN CHECKOUT
	if ($order === FALSE) {
		watchdog('setefi2', 'ERROR - ORDER NOT IN CHECKOUT STATE OR NOT FOUND, received response !descrizione_esito for payment from gateway but order #!trackid is not in "in_checkout" state', array('!trackid' => $trackid),WATCHDOG_ERROR);
		drupal_set_message(t('WARNING - ORDER #!trackid NOT FOUND, Please Retry.',array('!trackid'=>$trackid)),'warning');
		drupal_goto('cart');
	}
	// ERROR - PAYMENT NOT OK
	if ($responsecode  != "000") {
		uc_order_comment_save($order->order_id, 0, t('ERROR - PAYMENT NOT OK, response: !responsecode', array('!responsecode' => $responsecode)), 'admin');
		watchdog('setefi2', 'ERROR - PAYMENT NOT OK, response: !responsecode', array('!responsecode' => $responsecode),WATCHDOG_ERROR);
		drupal_set_message(t('ERROR - PAYMENT NOT COMPLETED for order #!trackid. Please Retry.',array('!trackid'=>$trackid)),'error');
		drupal_goto('cart');
	} else { // NOTICE - PAYMENT OK
		$payed=TRUE;
	}
	// ACTIONS AFTER PAYMENT
	if ($payed) {
		watchdog('setefi2', 'NOTICE - PAYMENT OK, response: !responsecode, order #!trackid, amount !purchase_amount', array('!responsecode' => $responsecode,'!trackid'=>$trackid,'!purchase_amount'=>$results['purchase_amount']),WATCHDOG_NOTICE);
		$comment = t('Payment approved by Setefi, order #!trackid, auth. code: !auth, payment id: !paymentID', array('!trackid' => $trackid,'!auth'=>$auth, '!paymentID' =>$paymentID));
		uc_payment_enter($order->order_id, 'setefi2', $results['purchase_amount'], 0, NULL, $comment);
		// Empty that cart...
		uc_cart_empty($cart_id);
		uc_order_save($order);
		// Set session variable to let Ubercart complete the order
		$_SESSION['do_complete'] = TRUE;
		$_SESSION['setefi2_order_id'] = $trackid;
		$order = uc_order_load($_SESSION['setefi2_order_id']);
		uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');

		$reply = 'REDIRECT=' . 'http://'.$_SERVER['HTTP_HOST'].base_path().'cart/setefi2/complete'; //URL di ritorno" . $paymentID;
		return $reply;
	}
}


function uc_setefi2_complete() {
	if (!$_SESSION['do_complete']) {
		drupal_goto('cart');
		exit();
	} else {
		$order = uc_order_load($_SESSION['setefi2_order_id']);
		$output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
		
		$page = variable_get('uc_cart_checkout_complete_page', '');
		if (!empty($page)) drupal_goto($page);
		
		$_SESSION['do_complete'] = FALSE;
		return $output;
	}
}


function uc_setefi2_error() {
	drupal_set_message(t("ERROR WITH CREDIT CARD PAYMENT GATEWAY.<br/>Please retry or call the shop support."))
	drupal_goto('cart');
}

